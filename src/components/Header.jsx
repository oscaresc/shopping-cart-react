import { Filters } from './Filters.jsx'

export function Header() {
    return (
        <>
            <h1>Carrito de compras</h1>
            <Filters />
        </>
    );
}