import './Cart.css'
import { useId } from "react"
import { CartIcon, ClearCartIcon } from "./Icons"
import { useCart } from "../hooks/useCart.js"

function CartItem({ title, price, thumbnail, quantity, addToCart }) {
    return (
        <li>
            <img src={ thumbnail } alt={title} />
            <div>
                <strong>{ title }</strong> - ${ price }
            </div>
            <footer>
                <small>
                    Qty: { quantity }
                </small>
                <button onClick={addToCart}>+</button>
            </footer>
        </li>
    )
}

export function Cart() {
    const cartCheckboxId = useId()
    const { cart, addToCart, clearCart } = useCart()
    
    return (
        <>
            <label className="cart-button" htmlFor={cartCheckboxId}>
                <CartIcon />
            </label>
            <input type="checkbox" id={cartCheckboxId} hidden/>

            <aside className="cart">
                <ul>
                    {
                        cart.map(item => {
                            return <CartItem 
                                key={item.product.id}
                                addToCart={() => addToCart(item.product)}
                                {...item.product}
                                quantity={item.quantity}
                            />
                        })
                    }
                </ul>

                <button onClick={clearCart}>
                    <ClearCartIcon />
                </button>
            </aside>

       </>
    )
}