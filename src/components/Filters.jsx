import { useId, useContext } from 'react'
import './Filters.css'
import { FiltersContext } from '../context/FiltersContext';

export function Filters() {
    const { filters, setFilters } = useContext(FiltersContext)

    const minPriceFilterId = useId()
    const categoryFilterId = useId()

    const handleMinPrinceChange = (event) => {
        setFilters(prevState => ({
            ...prevState,
            minPrice: event.target.value
        }))
    }

    const handleCategoryChange = (event) => {
        setFilters(prevState => ({
            ...prevState,
            category: event.target.value
        }))
    }

    return (
        <section className="filters">
            <div>
                <label htmlFor={minPriceFilterId}>Precio mínimo:</label>
                <input 
                    type="range" 
                    id={minPriceFilterId}
                    min="0" 
                    max="1000"
                    value={filters.minPrice}
                    onChange={handleMinPrinceChange}/>
            </div>
            <span>${filters.minPrice}</span>

            <div>
                <label htmlFor={categoryFilterId}>Categoría:</label>
                <select 
                    id={categoryFilterId} 
                    onChange={handleCategoryChange}>

                    <option value="all">Todas</option>
                    <option value="fragrances">Perfumes</option>
                    <option value="laptops">Notebooks</option>
                    <option value="smartphones">Celulares</option>
                    <option value="home-decoration">Hogar</option>
                </select>
            </div>
        </section>
    );
}