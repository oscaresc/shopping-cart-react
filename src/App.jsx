import { Products } from "./components/Products.jsx"
import { products as initialProducts } from "../data/products.json"
import { useState } from "react"
import { Header } from './components/Header.jsx'
import { useFilters } from './hooks/useFilters.js'
import { Cart } from "./components/Cart.jsx"
import { CartContext, CartContextProvider } from "./context/CartContext.jsx"

function App() {
    const [products] = useState(initialProducts)
    const {filterProducts}= useFilters()
    const filteredProducts = filterProducts(products)
    
    return (
        <>
            <CartContextProvider>
                <Header />
                <Cart />
                <Products products={filteredProducts} />
            </CartContextProvider>
        </>
    );
}

export default App;