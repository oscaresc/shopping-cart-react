# Enunciado

Ecommerce

- [x] Muestra una lista de productos que vienen de un JSON
- [x] Añade un filtro por categoría
- [x] Añade un filtro por precio

[x] Haz uso de useContext para evitar pasar props innecesarias.

Carrito:

- [X] Haz que se puedan añadir los productos a un carrito.
- [X] Haz que se puedan eliminar los productos del carrito.
- [X] Haz que se puedan modificar la cantidad de productos del carrito.
